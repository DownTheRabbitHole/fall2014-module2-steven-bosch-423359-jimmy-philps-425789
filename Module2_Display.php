<?php
session_start();
//Make sure user is still in session.
if (!isset($_SESSION['username'])){
        header("Location: Module2_Login.php");
        exit;
    }

//If delete is canceled.
if (isset($_GET['delete'])){
        header("Location: Module2_Files.php");
        exit;
}
$username = $_SESSION['username'];
//Check for files to download.
if (isset($_GET['download'])){
    $toDownload = $_GET['download'];
    $downloadPath = sprintf("/srv/module2_private/%s/%s", $username, $toDownload);
    //Check if file exists.
    if (file_exists($downloadPath)){
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mime = $finfo->file($downloadPath);
        header("Content-Type: $mime");
        header("Content-disposition: attachment; filename=\"$toDownload\"");
        readfile("$downloadPath");
        header("Location: Module2_Files.php");
        exit;
    }
    
}


//Check for files to display.
if (isset($_GET['view'])){
    $filename = $_GET['view'];
    // Obtain the file name and user name from the session.
    $username = $_SESSION['username'];
    //Build the file path.
    $filepath = sprintf("/srv/module2_private/%s/%s", $username, $filename);
    // Get the mime type.
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $mime = $finfo->file($filepath);
    // Display the file.
    
    header("Content-Type: $mime");
    readfile($filepath);
    exit;
}




?>
